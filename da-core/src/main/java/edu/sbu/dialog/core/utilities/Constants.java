package edu.sbu.dialog.core.utilities;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Constants {
	public static final Integer NUMBER_OF_CHOICES=3;
	
	public static Map<String,Integer>choices=new HashMap<>();
	public static final String DATA_PATH = "/Users/prasoon/Documents/SpringSemester/524/dialog-assistant/data";
	public static final String TAGGER_PATH = "/Users/prasoon/Documents/SpringSemester/524/dialog-assistant/english-left3words-distsim.tagger";
	
	static {
		choices.put("one", 1);
		choices.put("two", 2);
		choices.put("three", 3);
		choices.put("1", 1);
		choices.put("2", 2);
		choices.put("3", 3);
	}
	
	public static void main(String[] args) {
		Constants c = new Constants();
	}
}
