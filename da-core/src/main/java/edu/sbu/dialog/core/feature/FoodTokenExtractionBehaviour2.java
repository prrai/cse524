package edu.sbu.dialog.core.feature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.sbu.dialog.core.dialogmanager.Context;
import edu.sbu.dialog.core.pojos.NLPResponse;
import edu.sbu.dialog.core.taskmanager.TaskManager;
import edu.sbu.dialog.core.utilities.Constants;

public class FoodTokenExtractionBehaviour2 implements Behaviour<NLPResponse,List<String>>{
	private TaskManager taskManager;
	Map<String, Integer> choices = new HashMap<String, Integer>();
	Set<String> foodTokenSet;

	public FoodTokenExtractionBehaviour2(TaskManager taskManager) {
		choices.put("one", 1);
		choices.put("two", 2);
		choices.put("three", 3);
		choices.put("1", 1);
		choices.put("2", 2);
		choices.put("3", 3);
		this.taskManager = taskManager;
		foodTokenSet = taskManager.getAllFoodTokens();
	}
	
	private List<String> stringifyMap(Map<String,Map<String,List<String>>> nlpMap, String root) {
		Map<String,List<String>> root_map = nlpMap.get(root);
		List<String> result = new ArrayList<>();
		if (root_map != null) {
			if (root_map.containsKey("amod")) {
				result.addAll(root_map.get("amod"));
			}
			if (root_map.containsKey("compound")) {
				result.addAll(root_map.get("compound"));
			} 
		}
		result.add(root);
		return result;
	}
	
	private static void combine(List<List<String>> combs, List<String> comb, int start, int n, int k, List<String> items) {
		if(k==0) {
			combs.add(new ArrayList<String>(comb));
			return;
		}
		for(int i=start;i<n;i++) {
			comb.add(items.get(i));
			combine(combs, comb, i+1, n, k-1, items);
			comb.remove(comb.size()-1);
		}
	}
	
	private List<List<String>> getCombinations(int k, int n, List<String> items) {
		List<List<String>> combs = new ArrayList<List<String>>();
		combine(combs, new ArrayList<String>(), 0, n, k, items);
		return combs;
	}
	
	
	private List<String> getTokensFromMap(Map<String,Map<String,List<String>>> nlpMap) {
		List<String> tokens = null;
		for (Map.Entry<String,Map<String,List<String>>> entry : nlpMap.entrySet()) {
			List<String> candidates = new ArrayList<>();
			candidates.add(entry.getKey());
			List<String> items=taskManager.lookUpFoodMatches(candidates);
			if (items != null && items.size() > 0) {
				candidates = stringifyMap(nlpMap, entry.getKey());
				// We have a dobj now that has non zero results
				if (candidates.size() == 1) {
					return candidates;
				}
				int index = 0;
				// First try to do [amod] [[compounds]] [dobj] combinations
				while (index != candidates.size() - 1) {
					List<String>currentCandidates = new ArrayList<>(candidates.subList(index, candidates.size()));
					items = taskManager.lookUpFoodMatches(currentCandidates);
					if (items.size() > 0)
						return currentCandidates;
					index++;
				}
				// If serial string doesn't match, we try all combinations, in decreasing sizes
				candidates.remove(entry.getKey());
				List<String> bestList = new ArrayList<>();
				for (int i = candidates.size() - 1; i >= 1; i--) {
					List<List<String>> currentCombinations = getCombinations(i, candidates.size(), candidates);
					int maxMatches = 0;
					
					for (List<String> currentList : currentCombinations) {
						currentList.add(entry.getKey());
						items=taskManager.lookUpFoodMatches(currentList);
						if (maxMatches < items.size()) {
							maxMatches = items.size();
							bestList = new ArrayList<>(currentList);
						}
						System.out.println(currentList);
						System.out.println(items.size());
						currentList.remove(entry.getKey());
					}
					if (maxMatches > 0)
						return bestList;
				}
			}
		}
		return tokens;
	}
	
	private List<String> getTokensFromStr(List<String> nlpTokens) {
		List<String> tokens = new ArrayList<>();
		List<String> frequentTokens = taskManager.getCurrentFrequentTokens();
		List<String> items = taskManager.getCurrentFilteredList();
		List<Integer> choices = new ArrayList<>();
		if (items != null && items.size() > Constants.NUMBER_OF_CHOICES) {
			for (String str : nlpTokens) {
				if (str.toLowerCase().equals("one") || str.equals("1") || str.toLowerCase().equals("first"))
					choices.add(0);
				else if (str.toLowerCase().equals("two") || str.equals("2") || str.toLowerCase().equals("second"))
					choices.add(1);
				else if (str.toLowerCase().equals("three") || str.equals("3") || str.toLowerCase().equals("third"))
					choices.add(2);
				else if (frequentTokens.contains(str))
					tokens.add(str);
			}
			for (Integer choice : choices) {
				if (frequentTokens.size() > choice)
					tokens.add(frequentTokens.get(choice));
			}
		}
		else {
			for (String str : nlpTokens) {
				if (str.toLowerCase().equals("one") || str.equals("1") || str.toLowerCase().equals("first"))
					tokens.add("1");
				else if (str.toLowerCase().equals("two") || str.equals("2") || str.toLowerCase().equals("second"))
					tokens.add("2");
				else if (str.toLowerCase().equals("three") || str.equals("3") || str.toLowerCase().equals("third"))
					tokens.add("3");
			}
		}
		System.out.println("Sending tokens: " + tokens);
		return tokens;
	}
	
	@Override
	public List<String> doAction(Context context, NLPResponse input) {
		List<String> tokens=null;
		Map<String,Map<String,List<String>>> nlpMap;
		List<String> nlpTokens;

		if (input != null) {
			nlpMap = input.getMap();
			nlpTokens = input.getUserInputParsedTokens();
			if (nlpMap != null)
				tokens = getTokensFromMap(nlpMap);
			else if(nlpTokens != null)
				tokens = getTokensFromStr(nlpTokens);
		}
		return tokens;
	}
	
	public void setTaskManager(TaskManager taskManager) {
		this.taskManager = taskManager;
	}

}
