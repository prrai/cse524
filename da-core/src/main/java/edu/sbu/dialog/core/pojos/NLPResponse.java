package edu.sbu.dialog.core.pojos;

import java.util.List;
import java.util.Map;

public class NLPResponse {
	Map<String,Map<String,List<String>>> map;
	List<String> userInputTokenized;
	
	public Map<String, Map<String, List<String>>> getMap() {
		return map;
	}
	public void setMap(Map<String, Map<String, List<String>>> map) {
		this.map = map;
	}
	
	public List<String> getUserInputParsedTokens() {
		return userInputTokenized;
	}
	public void setUserInputParsedTokens(List<String> tokens) {
		this.userInputTokenized = tokens;
	}
}
