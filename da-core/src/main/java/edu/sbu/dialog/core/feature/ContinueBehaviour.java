package edu.sbu.dialog.core.feature;

import java.util.Arrays;
import java.util.List;

import edu.sbu.dialog.core.dialogmanager.Context;
import edu.sbu.dialog.core.pojos.NLPResponse;
import edu.sbu.dialog.core.pojos.StateEnum;
import edu.sbu.dialog.core.taskmanager.TaskManager;
import edu.sbu.dialog.core.utilities.Messages;
import edu.sbu.dialog.core.utilities.POSTags;

public class ContinueBehaviour implements Behaviour<NLPResponse, String>{
	private TaskManager taskManager;
	
	public ContinueBehaviour(TaskManager taskManager) {
		this.taskManager=taskManager;
	}
	
	private boolean proceed(List<String> tokens) {
		String[] positive = {"yes", "yeah", "yea", "ok", "sure", "yep", "ya"};
		List<String> positiveList = Arrays.asList(positive);
		for (int i = 0; i < tokens.size(); i++) {
			if (positiveList.contains(tokens.get(i).toLowerCase()))
				return true;
		} 
		return false;
	}
	
	@Override
	public String doAction(Context context, NLPResponse input) {
		String textToUser;
		List<String> userTokens = input.getUserInputParsedTokens();
		if (userTokens == null || userTokens.size() == 0)
			textToUser = Messages.CONTINUE_REPEAT;
		else if(proceed(userTokens))
			textToUser = Messages.ASK_CLIENT;
		else
			textToUser=Messages.GREET;
		return textToUser;
	}
}
