package edu.sbu.dialog.core.feature;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.sbu.dialog.core.dialogmanager.Context;
import edu.sbu.dialog.core.pojos.NLPResponse;
import edu.sbu.dialog.core.pojos.StateEnum;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class NLPBehaviour2 implements Behaviour<String,NLPResponse>{
	private LexicalizedParser lp;

	@Override
	public NLPResponse doAction(Context context, String input) {
		NLPResponse response = new NLPResponse();
		if (context.getPreviousState() == null || context.getPreviousState().getState() == StateEnum.INITIAL) {
			response.setMap(generateDependencyMap(input));
		}
		else {
			response.setUserInputParsedTokens(tokenizeUserInput(input));
		}
		return response;
	}
	
	public NLPBehaviour2() {
		lp = LexicalizedParser.loadModel(
				"edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz",
				"-maxLength", "80", "-retainTmpSubcategories");
	}
	
	private static String cleanWord(String s) {
	    Pattern pattern = Pattern.compile("[^a-z A-Z]");
	    Matcher matcher = pattern.matcher(s);
	    String result = matcher.replaceAll("");
	    return result;
	 }
	
	private Map<String, Map<String, List<String>>> generateDependencyMap(String text) {
		Map<String, Map<String, List<String>>> rootDep = new HashMap<String, Map<String, List<String>>>();
		Map<String, List<String>> dobjMap;
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		String[] sent = text.split(" ");
		Tree parse = lp.apply(Sentence.toWordList(sent));
		GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
		Collection<TypedDependency> tdl = gs.typedDependencies();
		System.out.format("Typed dependencies for user input : \"%s\" are: %s\n", text, tdl);
		for(Iterator<TypedDependency> iter = tdl.iterator(); iter.hasNext();) {
			TypedDependency var = iter.next();
			if (var.reln().toString().equals("dobj")) {
				dobjMap = new HashMap<String, List<String>>();
				for(Iterator<TypedDependency> iter2 = tdl.iterator(); iter2.hasNext();) {
					TypedDependency var2 = iter2.next();
					if (var2.gov().pennString().toLowerCase().equals(
									var.dep().pennString().toString().toLowerCase())) {
						if (var2.reln().toString().equals("amod")) {
							if(!dobjMap.containsKey("amod"))
								dobjMap.put("amod", new ArrayList<>());
							dobjMap.get("amod").add(cleanWord(var2.dep().pennString().trim()));
						}
						if (var2.reln().toString().toLowerCase().equals("nn")) {
							if(!dobjMap.containsKey("compound"))
								dobjMap.put("compound", new ArrayList<>());
							dobjMap.get("compound").add(cleanWord(var2.dep().pennString().trim()));
						}
					}
				}
				rootDep.put(cleanWord(var.dep().pennString().trim()), dobjMap);
			}
		}
		System.out.format("Dependency map for user input : \"%s\" is: %s\n", text, rootDep);
		return rootDep;
	}
	
	private List<String> tokenizeUserInput(String input) {
		List<String> tokens = new ArrayList<>();
		Collections.addAll(tokens, input.split(" "));
		for (int i = 0; i < tokens.size(); i++) {
			if (tokens.get(i).equals("1") || tokens.get(i).equals("2") || tokens.get(i).equals("3"))
				tokens.set(i, tokens.get(i));
			else
				tokens.set(i, cleanWord(tokens.get(i)));
		}
		return tokens;
	}
}
