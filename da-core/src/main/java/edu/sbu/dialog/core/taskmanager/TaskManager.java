package edu.sbu.dialog.core.taskmanager;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import edu.sbu.dialog.core.data.filters.Filter;
import edu.sbu.dialog.core.data.filters.FrequencyFilter;
import edu.sbu.dialog.core.ds.FileDS;
import edu.sbu.dialog.core.ds.FileDSImpl;
import edu.sbu.dialog.core.feature.NLPBehaviour2;
import edu.sbu.dialog.core.utilities.Constants;

/**
 * This class decouples the finite state automaton from backend logic. 
 * This makes easier for the backend logic evolve independent of the 
 * automaton logic. 
 * @author kedar
 *
 */
public class TaskManager {
	private Filter filter;
	private FileDS ds;
	
	public TaskManager() {
		filter=new FrequencyFilter();
		ds=new FileDSImpl();
	}

	public TaskManager(Filter filter) {
		super();
		this.filter = filter;
	}
	
	public List<String> getCurrentFilteredList(){
		return filter.getCurrentFilteredList();
	}
	
	public List<String> getPreviousTokens(){
		return filter.getPreviousTokens();
	}
	
	public List<String> getNextFrequentTokens(List<String> currentTokens){
		return filter.getNextFrequentTokens(currentTokens);
	}
	
	public List<String> filterFoodItemsBy(List<String> tokens){
		return filter.filter(tokens);
	}
	
	public String getCalories(String fooditem) {
		File loc = new File(Constants.DATA_PATH);
		File csvData = ds.searchFile(loc, fooditem);
		Charset utf8charset = Charset.forName("ISO-8859-1"); 
		CSVParser parser = null;
		try {
			parser = CSVParser.parse(csvData, utf8charset, CSVFormat.EXCEL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 for (CSVRecord csvRecord : parser) {
			 if (csvRecord.toString().contains("Energy")) {
				 return csvRecord.get(2);
			 }
		 }
		 return "";
	}	
	
	/**
	 * Method to get matches of tokens list against DB entries
	 * without modifying filteredList.
	 * @return
	 */
	public List<String> lookUpFoodMatches(List<String> tokens) {
		List<String> matches=new ArrayList<String>();
		if(tokens==null||tokens.size()==0)
			return matches;
		List<String> initialList = new ArrayList<>(this.getInitialList());
		for(String file:initialList) {
			List<String> fileTokens=Arrays.asList(file.split("-"));
			
			if(fileTokens.containsAll(tokens))
				matches.add(file);
		}
		return matches;
	}
	
	public List<String> getCurrentFrequentTokens(){
		return filter.getCurrentFrequentTokens();
	}
	
	public Set<String> getAllFoodTokens(){
		return filter.getAllFoodTokens();
	}
	
	public void initializeFilterList(){
		filter.intializeFilterList();
	}
	
	public List<String> getInitialList() {
		return filter.getInitialList();
	}
}
