package edu.sbu.dialog.core.utilities;

public class Messages {
	public static final String FIRST_ASK_CLIENT="Hi, please tell me what you ate today.";
	public static final String ASK_CLIENT="Please tell me what you ate.";
	public static final String GREET="Thank You, Have a nice day";
	public static final String REPEAT="Sorry, could not understand, please repeat";
	public static final String SELECT_CHOICE="Say ONE for {1}, TWO for {2}, THREE for {3}";
	public static final String INFORM_NUTRIENTS="100 grams of {1} contains {2} kilo calories. \n\n Would you like to continue";
	public static final String CONTINUE_REPEAT="Did not understand, say YES to continue, NO to exit";
}
